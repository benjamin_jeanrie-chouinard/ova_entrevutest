projet de Benjamin Jeanrie-Chouinard

!!Pour lancer les applications, il faud que vous ayer télécharger Node.js.
Vous pouvez suivre ce guide qui explique très bien:
https://radixweb.com/blog/installing-npm-and-nodejs-on-windows-and-mac

backend: node.js avec express
frontend: vue.js

Ouvrez une fenetre powershell et aller sur l'emplacement du fichier
"backend" puis lancer la commande: 
npm install
suivi de
node app.js

l'api sera connecté au port http://localhost:3000/

Ouvrez une autre fenetre powershell et aller sur
l'emplacement du fichier frontend puis lancer la commande: 
npm install
suivi de
npm run serve

lancer un navigateur web puis aller à l'url: http://localhost:8080/
